using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class BigBangScript : MonoBehaviour
{
    public GameObject BigBang;
    public GameObject QuantumGravity;
    public GameObject EarlyStageBar;
    public Image BigBangImage;

    public ParticleSystem BigBangPS;
    public ParticleSystem BackgroundPS;

    public TextMeshProUGUI EpochText;

    private Animator BigBangButtonPress;

    private void Start()
    {
        BigBangButtonPress = GetComponent<Animator>();
    }

    public void BigBangButton()
    {
        EpochText.text = "Planck Epoch";
        BigBangPS.Play();
        BigBangButtonPress.SetTrigger("BigBangButtonPress");
        StartCoroutine(StartPlanckEpoch());
        //BigBangImage.enabled = false;
    }

    IEnumerator StartPlanckEpoch()
    {
        yield return new WaitForSeconds(3);
        BackgroundPS.Play();
        QuantumGravity.SetActive(true);
        EarlyStageBar.SetActive(true);
        BigBang.SetActive(false);
    }
}
