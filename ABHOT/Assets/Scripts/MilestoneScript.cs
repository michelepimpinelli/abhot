using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class MilestoneScript : MonoBehaviour
{
    public Button GatherButton;
    public Button PurchaseButton;
    
    public int MilestoneNumber;
    public TextMeshProUGUI MilestoneNumberTmPro;
    public float MilestoneCost;
    public float MilestoneInertiaProd;
    public float PlanckTimeProd;
    public float QuectoSecondsProd;
    public float MilestoneMultiplier = 1;
    public float CostRatio;

    public float TimeToNextMilestone;
    public float qsTimeToNextMilestone;

    public GameObject NextMileStone;

    private void Update()
    {
        if(DataManager.Instance.Inertia < MilestoneCost)
        {
            PurchaseButton.enabled = false;
        }
        else PurchaseButton.enabled=true;

        if(MilestoneNumber == 0)
        {
            GatherButton.enabled = false;
        }
        else GatherButton.enabled=true;

        MilestoneNumberTmPro.text = MilestoneNumber.ToString();

        if(MilestoneNumber>=5)
        {
            DataManager.Instance.Inertia += MilestoneInertiaProd * MilestoneNumber * MilestoneMultiplier * Time.deltaTime;
            DataManager.Instance.PlanckTime += PlanckTimeProd * MilestoneNumber * MilestoneMultiplier * Time.deltaTime;
            DataManager.Instance.QuectoSeconds += QuectoSecondsProd * MilestoneNumber * Time.deltaTime;
        }
        if(MilestoneNumber >= 10)
        {
            MilestoneMultiplier = Mathf.Floor(MilestoneNumber/10) +1;
        }

        if(DataManager.Instance.PlanckTime >= TimeToNextMilestone && DataManager.Instance.TotalQuectoSeconds >= qsTimeToNextMilestone)
        {
            NextMileStone.SetActive(true);
        }
    }
    

    public void MilestonePurchaseButton()
    {
        DataManager.Instance.Inertia -= MilestoneCost;
        MilestoneNumber++;
        MilestoneCost *= 1.5f;
    }


    public void MilestoneButton()
    {
        DataManager.Instance.Inertia += MilestoneInertiaProd * MilestoneNumber * MilestoneMultiplier;
        DataManager.Instance.PlanckTime += PlanckTimeProd * MilestoneNumber * MilestoneMultiplier;
        DataManager.Instance.QuectoSeconds += QuectoSecondsProd * MilestoneNumber * MilestoneMultiplier;
    }
}
