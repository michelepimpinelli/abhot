using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Unity.VisualScripting;

public class DataManager : MonoBehaviour
{
    public static DataManager Instance;

    public float Inertia;
    public float PlanckTime;
    public double QuectoSeconds;
    double QuectoSecondsConvertion;
    public double TotalQuectoSeconds;

    public TextMeshProUGUI InertiaTmPro;
    public TextMeshProUGUI CurrentTimeTmPro;
    public TextMeshProUGUI EpochTmPro;

    public Animator QGTranslation;

    private bool GUEEpoch;
    private bool InfEpoch;
    private bool EleweakEpoch;

    private float Timer = 0.3f;

    private void Update()
    {
        if (Timer > 0)
        {
            Timer -= Time.deltaTime;
        }
        else
        {
            if (Inertia < 1000)
            {
                InertiaTmPro.text = Inertia.ToString("F2");
            }
            else if (Inertia >= 1000 && Inertia < 10000)
            {
                InertiaTmPro.text = Inertia.ToString("F0");
            }
            else if(Inertia >= 10000 && Inertia < 10000000)
            {
                InertiaTmPro.text = (Inertia/1000).ToString("F2") + " K";
            }
            else if(Inertia >= 10000000 && Inertia < 10000000000)
            {
                InertiaTmPro.text = (Inertia / 1000000).ToString("F2") + " M";
            }
            else if(Inertia >= 10000000000 && Inertia < 10000000000000)
            {
                InertiaTmPro.text = (Inertia / 1000000000).ToString("F2") + " G";
            }
            else if(Inertia >= 10000000000000 && Inertia < 10000000000000000)
            {
                InertiaTmPro.text = (Inertia / 1000000000000).ToString("F2") + " T";
            }
            else if(Inertia >= 10000000000000000 && Inertia < 10000000000000000000)
            {
                InertiaTmPro.text = (Inertia / 1000000000000000).ToString("F2") + " P";
            }

            if (PlanckTime < 1000)
            {
                CurrentTimeTmPro.text = PlanckTime.ToString("0.###") + "tP";
            }
            else if (PlanckTime >= 1000 && PlanckTime < 1000000)
            {
                CurrentTimeTmPro.text = PlanckTime.ToString("F0") + "tP";
            }
            else if (PlanckTime >= 1000000 && PlanckTime < 1000000000)
            {
                CurrentTimeTmPro.text = (PlanckTime / 1000000).ToString("0.##") + "mln tP";
            }
            else if (PlanckTime >= 1000000000 && PlanckTime < 1000000000000)
            {
                CurrentTimeTmPro.text = (PlanckTime / 1000000000).ToString("0.##") + "bln tP";
            }
            else if (PlanckTime >= 1000000000000 && TotalQuectoSeconds < 1000)
            {
                CurrentTimeTmPro.text = TotalQuectoSeconds.ToString("0.###") + "qs";
            }
            else if (TotalQuectoSeconds >= 1000 && TotalQuectoSeconds < 1000000)
            {
                CurrentTimeTmPro.text = TotalQuectoSeconds.ToString("F0") + "qs";
            }
            else if(TotalQuectoSeconds >= 1000000)
            {
                CurrentTimeTmPro.text = TotalQuectoSeconds.ToString("#E+##") + "qs";
            }
            Timer = 0.3f;
        }
        if(PlanckTime >= 1 && !GUEEpoch)
        {
            GUEEpoch = true;
            EpochTmPro.text = "Grand Unification Epoch";
            QGTranslation.SetTrigger("GUEStart");
        }

        if (PlanckTime >= 100000000 && !InfEpoch)
        {
            InfEpoch = true;
            EpochTmPro.text = "Inflationary Epoch";
        }

        if(TotalQuectoSeconds >= 0.01f && !EleweakEpoch)
        {
            EleweakEpoch = true;
            EpochTmPro.text = "Electroweak Epoch";
        }

        QuectoSecondsConvertion = PlanckTime /(1e+14);
        print(QuectoSecondsConvertion);
        TotalQuectoSeconds = QuectoSeconds + QuectoSecondsConvertion;
    }

    private void Awake()
    {
        Instance = this;
    }

    public void TestingInertia()
    {
        Inertia += 1000000000000000;
    }
    public void TestingTime()
    {
        PlanckTime += 100000000000;
    }
}
